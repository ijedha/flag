# Flag Redesign for Ijeða

## Files

The file titled 'flag' is the flag itself.

The file titled 'profile' is for profile pictures, and is designed for circular profile pictures only. The extra margins on the top and bottom of the right side are cropped off when cut in a circle.

The file titled 'icon' is for square icons that aren't circular.

The file titled 'icon2' is just a neat design with a transparent background.

~~In Discord, I think you can upload svg files for a profile picture, and so try using those first.~~
Discord does not support svg for profile picture, use the picture in the png/ folder